﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson_IEnumerable
{
    static class MicMath
    {
        public static int Sum(IEnumerable source)
        {
            int sum = 0;
            foreach (int item in source)
                sum += item;
            return sum;
        }

       
    }
}