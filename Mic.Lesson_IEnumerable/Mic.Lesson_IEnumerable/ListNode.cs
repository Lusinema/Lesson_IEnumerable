﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson_IEnumerable
{
    class ListNode : IEnumerable
    {
        public ListNode(int item)
        {
            value = item;
        }
        public int value;
        public ListNode next;
        private ListNode _currentNode;

        public void Add(int item)
        {
            if (_currentNode == null)
            {
                next = new ListNode(item);
                _currentNode = next;
            }
            else
            {
                _currentNode.next = new ListNode(item);
                _currentNode = _currentNode.next;
            }
        }
        public override string ToString()
        {
            return value.ToString();
        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        private struct Enumerator : IEnumerator
        {
            public Enumerator(ListNode root)
            {
                _node = root;
                Current = root;
            }
            public object Current { get; private set; }
            private ListNode _node;
            public bool MoveNext()
            {
                if (_node == null)
                    return false;

                Current = _node.value;
                _node = _node.next;
                return true;
            }

            public void Reset()
            {
                _node = null;
            }
        }
    }



}
