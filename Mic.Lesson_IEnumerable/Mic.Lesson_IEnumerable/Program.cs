﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson_IEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new ListNode(10);
            root.Add(20);
            root.Add(1000);
            root.Add(40);
            root.Add(50);

            foreach (var item in root)
            {

            }

            IEnumerator enumerator = root.GetEnumerator();
            while (enumerator.MoveNext())
            {
                int item = (int)enumerator.Current;
            }
            enumerator.Reset();

            Console.ReadLine();

        }
    }
}
